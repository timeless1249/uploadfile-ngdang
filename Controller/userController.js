import User from "../Model/userModel";

function createUser(req, res) {
    User.findOne({ username: req.body.username }, function (err, data) {
        if (err) {
            res.send("Loi mongodb");
        }
        else if (data == undefined) {
            const newUser = new User({
                username: req.body.username,
                fullname: req.body.fullname,
                password: req.body.password
            })
            newUser.save(function (err) {
                if (err) {
                    res.send("Loi mongodb");
                } else {
                    res.send("Dang ky thanh cong")
                }
            })
        }
        else {
            res.send("Da ton tai username")
        }
    })
}

function loginUser(req, res) {
    User.findOne({
        username: req.body.username,
        password: req.body.password
    }, function (err, data) {
        if (err) {
            res.send("Loi mongodb");
        }
        else if (data == undefined) {
            res.send("Username hoặc password không đúng");
        }
        else {
            res.send("Dang nhap thanh cong")
        }
    })
}

export default {
    createUser,
    loginUser
}