import SocketIo from "socket.io";
import User from "../Model/userModel";
import ChatRoom from "../Model/chatRoomModel";

module.exports.listen = function (app) {
    var io = SocketIo.listen(app)

    io.on("connection", function (socket) {
        // const i = socket.id
        // var list = socket.adapter.rooms[i]
        // console.log(list.length)
        // for (const key in list) {
        //     console.log(list[i])
        // }
        let friend = [];

        console.log("Co nguoi ket noi:" + socket.id);

        socket.on("disconnect", function () {
            for (const key in friend) {
                socket.to(friend[key].chatRoom).emit("Room-send-logout", friend[key].chatRoom);
            }
        });

        socket.on("Client-send-chat", function (data) {
            var a = {
                value: data.value,
                sender: data.sender
            }
            io.in(data.chatRoom).emit("Server-send-chat", a);
        })

        function joinChat(data) {
            var list = data.friend;
            for (const key in list) {
                socket.join(list[key].chatRoom);
                socket.to(list[key].chatRoom).emit("Room-send-login", list[key].chatRoom);

                if (socket.adapter.rooms[list[key].chatRoom].length > 1) {
                    friend.push({
                        friendId: list[key].friendId,
                        chatRoom: list[key].chatRoom,
                        fullname: list[key].fullname,
                        check: 1
                    })
                } else {
                    friend.push({
                        friendId: list[key].friendId,
                        chatRoom: list[key].chatRoom,
                        fullname: list[key].fullname,
                        check: 0
                    })
                }
            }
            socket.emit("Server-send-friend", friend);
        }


        socket.on("Client-send-token", function (data) {
            User.findOne({ username: data }, function (err, result) {
                if (err || result == undefined) {
                    socket.emit("Server-send-token-fail")
                }
                else {
                    socket.emit("Server-send-token-success", result)
                    joinChat(result)
                }
            })
        })


        socket.on("Client-send-dangky", function (data) {
            User.findOne({ username: data.username }, function (err, result) {
                if (err) {
                    socket.emit("Server-send-dangky-fail", "Loi Mongodb")
                }
                else if (result == undefined) {
                    const newUser = new User({
                        username: data.username,
                        fullname: data.fullname,
                        password: data.password,
                        friend: []
                    });
                    newUser.save(function (err, result1) {
                        if (err) {
                            socket.emit("Server-send-dangky-fail", "Loi mongodb")
                        } else {
                            socket.emit("Server-send-dangky-success")
                        }
                    })
                }
                else {
                    socket.emit("Server-send-dangky-fail", "Đã tồn tại Username")
                }
            })
        })

        socket.on("Client-send-login", function (req) {
            User.findOne({
                username: req.username,
                password: req.password
            }, function (err, result) {
                if (err) {
                    socket.emit("Server-send-login-fail", "Loi mongodb");
                }
                else if (result == undefined) {
                    socket.emit("Server-send-login-fail", "Username hoac password khong dung");
                }
                else {
                    socket.emit("Server-send-login-success", result)
                    joinChat(result)
                }
            })
        })
        socket.on("Goi-y-ket-ban", function (data) {
            User.aggregate(
                [{ $sample: { size: 5 } }],
                function (err, result) {
                    var list = [];
                    result.forEach(user => {
                        var listfriend = user.friend;
                        var check = 1;
                        if (user._id == data) {
                            check = 0
                        }
                        else {
                            for (var i = 0; i < listfriend.length; i++) {
                                if (listfriend[i].friendId == data) {
                                    check = 0;
                                }
                            }
                        }
                        if (check == 1) {
                            list.push(user)
                        }
                    });
                    socket.emit("Goi-y-ket-ban-done", list);
                }
            )
        })

        socket.on("Client-send-ketban", function (data) {
            const newChatRoom = new ChatRoom({
                user1: data.id1,
                user2: data.id2,
                message: []
            })

            newChatRoom.save(function (err, result) {
                if (err || result == undefined) {
                    socket.emit("Server-send-ketban-fail")
                }
                else {
                    User.updateOne({ _id: data.id1 }, {
                        $push: {
                            friend: [{
                                friendId: data.id2,
                                chatRoom: result._id,
                                fullname: data.name2
                            }]
                        }
                    }, function (err, value) {
                    })
                    User.updateOne({ _id: data.id2 }, {
                        $push: {
                            friend: [{
                                friendId: data.id1,
                                chatRoom: result._id,
                                fullname: data.name1
                            }]
                        }
                    }, function (err, value) {
                    })
                    socket.emit("Server-send-ketban", data.key);
                }
            })
        })

    });
}