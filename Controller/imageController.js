import Image from "../Model/imageModel";
import mongoose from "mongoose";
import multer from "multer";
import fs from "fs"

var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, "uploads/");
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	},
});

var upload = multer({
	storage: storage,
	//limits: { fileSize: 500000 },
	fileFilter: function (req, file, cb) {
		var a = file.mimetype.split("/");
		if (a[0] == "image") {
			cb(null, true);
		} else {
			cb(null, false);
		}
	},
}).array("avatar", 7);

function uploadImage(req, res) {
	upload(req, res, function (err) {
		if (err instanceof multer.MulterError) {
			res.send(err.code);
		} else if (err) {
			res.send(err.code);
		} else if (req.files == []) {
			res.send("upload that bai");
		} else {
			//res.send(req.files[0].originalname);
			let files = req.files;
			var image = [];
			for (var i = 0; i < files.length; i++) {
				image.push({ url: "/uploads/" + files[i].originalname });
			}
			Image.collection.insert(image, function (err) {
				if (err) {
					res.send("Loi Mongose");
				} else {
					res.send("upload thanh cong");
					for (var i = 0; i < files.length; i++) {
						res.sendFile(__dirname + "/uploads/" + files[i].originalname);
					}
				}
			});
		}
	});
}

function loadImage(req, res) {
	Image.find({}, function (err, data) {
		if (err) {
			res.json({
				mess: err
			})
		}
		else if (data == undefined) {
			res.json({
				mess: "Du lieu trong"
			})
		}
		else {
			res.send(data)
		}
	});
}

function deleteImage(req, res) {
	Image.findOneAndDelete({ _id: req.body._id }, function (err, data) {
		if (err) {
			res.send("Loi, khong the xoa file")
		}
		else if (data == undefined) {
			res.send("Khong tim thay file")
		}
		else {
			res.send("Xoa thanh cong")
			// fs.unlink("." + data.url, (err) => {
			// 	if (err) {
			// 		console.log(err)
			// 	}
			// })
		}
	})
}

function updateImage(req, res) {
	upload(req, res, function (err) {
		//res.send(req.files)
		if (err instanceof multer.MulterError) {
			res.send(err.code);
		} else if (err) {
			res.send(err.code);
		} else if (req.files == []) {
			res.send("upload that bai");
		} else {
			let file = req.files[0];
			Image.findOneAndUpdate({ _id: req.body._id }, { url: "/uploads/" + file.originalname }, function (err, result) {
				if (err) {
					res.send("Loi Mongodb");
				} else if (result == undefined) {
					res.send("khong tim thay du lieu")
				}
				else {
					res.send("Update thanh cong");
					// fs.unlink("." + result.url, (err) => {
					// 	if (err) {
					// 		console.log(err)
					// 	}
					// })
					res.sendFile(__dirname + "/uploads/" + file.originalname);
				}
			})
		}
	});
}

export default {
	uploadImage,
	loadImage,
	deleteImage,
	updateImage
};
