import mongose, { Schema } from "mongoose"

const userSch = new Schema({
    username: String,
    fullname: String,
    password: String,
    friend: [{
        friendId: { type: Schema.Types.ObjectId, ref: 'User' },
        fullname: String,
        chatRoom: { type: Schema.Types.ObjectId, ref: 'ChatRoom' }
    }]
})

export default mongose.model('User', userSch)