import mongose, { Schema } from "mongoose"

const chatRoomSch = new Schema({
    user1: { type: Schema.Types.ObjectId, ref: 'User' },
    user2: { type: Schema.Types.ObjectId, ref: 'User' },
    message: [{
        value: String,
        time: Date,
        sender: { type: Schema.Types.ObjectId, ref: 'User' }
    }]
})

export default mongose.model('ChatRoom', chatRoomSch)