import mongose, { Schema } from "mongoose"

const imageSch = new Schema({
    url: String,
})

export default mongose.model('Image', imageSch)