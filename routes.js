import express from "express";
import multer from "multer";
import imageController from "./Controller/imageController";
import userController from "./Controller/userController"

const router = express();


router.get("/viewImage", function (req, res) {
	res.sendFile("View/viewImage.html", { root: __dirname });
});

router.get("/", function (req, res) {
	res.sendFile("View/chatUser.html", { root: __dirname });
})


router.post("/uploadImage", imageController.uploadImage);

router.get("/loadImage", imageController.loadImage)

router.delete("/deleteImage", imageController.deleteImage)

router.post("/updateImage", imageController.updateImage)


router.post("/createUser", userController.createUser)

router.post("/loginUser", userController.loginUser)

export default router;