// var express = require('express')
import express from "express"
import mongoose from "mongoose"
import bodyParser from 'body-parser';
import routes from './routes'
import SocketIo from "./Controller/SocketCode"
import Http from "http";

var app = express();
var server = Http.Server(app);
SocketIo.listen(server);


mongoose
  .connect("mongodb://localhost:27017/test")
  .then(() => console.log("connect"))
  .catch(error => console.log('MONGO CONNECT FAIL'))


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', routes);
app.use('/uploads', express.static('uploads'))


server.listen(3000)

